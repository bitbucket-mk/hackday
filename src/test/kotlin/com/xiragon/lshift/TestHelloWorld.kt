package com.xiragon.lshift

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class TestHelloWorld {

    @Test
    fun `can get Hello World message`() {
        assertThat(HelloWorld.message(), equalTo("Hello, world!"))
    }
}