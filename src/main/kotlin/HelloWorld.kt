
object HelloWorld {
    fun message() = "Hello, world!"

    @JvmStatic
    fun main(args: Array<String>) {
        println(message())
    }
}